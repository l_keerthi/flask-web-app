import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from flask import Flask, request, jsonify, render_template, redirect, url_for, flash
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager, UserMixin, login_user,  login_required, logout_user, current_user
from flask_wtf import FlaskForm
from werkzeug.utils import secure_filename
from wtforms import SubmitField, StringField, PasswordField
from wtforms.validators import InputRequired, Length, ValidationError
from flask_bcrypt import Bcrypt
import pickle
import csv
import os
import pandas as pd
from matplotlib import pyplot
from matplotlib.figure import Figure


app = Flask(__name__)
db = SQLAlchemy(app)
bcrypt = Bcrypt(app)
app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///database.db"  # create the database instance and connect the app.
app.config['SECRET_KEY'] = "thisisasecretkey"

login_manager = LoginManager()        # handle the things when user logging in,loading user from ID's.
login_manager.init_app(app)
login_manager.login_view = "login"


@login_manager.user_loader          # It is used to reload the user object from the user ID stored in the session.
def load_user(user_id):
    return User.query.get(int(user_id))


class User(db.Model, UserMixin):      # create the User table in the database.db
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), nullable=False, unique=True)
    password = db.Column(db.String(80), nullable=False)


class SignupForm(FlaskForm):           # created signup form inherit from Flask form package
    username = StringField(validators=[InputRequired(), Length(
        min=4, max=20)], render_kw={"placeholder": "Username"})

    password = PasswordField(validators=[InputRequired(), Length(
        min=4, max=20)], render_kw={"placeholder": "Password"})

    submit = SubmitField("Signup")

    @staticmethod
    def validate_username(self, username):       # check the username is unique or not comparing the database query.
        existing_user_username = User.query.filter_by(
            username=username.data).first()

        if existing_user_username:
            raise ValidationError(
                flash('User name already exists'))


class LoginForm(FlaskForm):                       # created login form inherit from Flask form package
    username = StringField(validators=[InputRequired(), Length(
        min=4, max=20)], render_kw={"placeholder": "Username"})

    password = PasswordField(validators=[InputRequired(), Length(
        min=4, max=20)], render_kw={"placeholder": "Password"})

    submit = SubmitField("Login")


model = pickle.load(open("model.pkl", "rb"))       # Load the pickle file


@app.route('/upload')
@login_required
def home():
    return render_template('upload.html')


@app.route('/predict', methods=['POST'])
def predict():

    int_features = [int(x) for x in request.form.values()]
    final_features = [np.array(int_features)]
    prediction = model.predict(final_features)

    output = prediction[0]

    return render_template('index.html', prediction_text='Expense = {}'.format(output))


@app.route('/', methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():                     # used to check  if the user is in the database or not.
        user = User.query.filter_by(username=form.username.data).first()
        if not user:
            flash('Please sign up before!')
            return redirect(url_for('signup'))
        if user:                                                         # then check their password is correct or not.
            if not bcrypt.check_password_hash(user.password, form.password.data):
                flash('Please check your login details and try again.')
                return redirect(url_for('login'))
        login_user(user)
        return redirect(url_for('home'))
    return render_template('login.html', form=form)


@app.route('/signup', methods=['GET', 'POST'])
def signup():
    form = SignupForm()

    if form.validate_on_submit():
        hashed_password = bcrypt.generate_password_hash(form.password.data)     # bcrypt use to hash the passwords.
        new_user = User(username=form.username.data, password=hashed_password)
        db.session.add(new_user)
        db.session.commit()
        return redirect(url_for('login'))

    return render_template('signup.html', form=form)


# @app.route('/logout', methods=['GET', 'POST'])
# @login_required
# def logout():
    # logout_user()
    # return redirect(url_for('login'))

@app.route('/upload', methods=['GET', 'POST'])
def upload_file():
    return render_template('upload.html')


@app.route('/data', methods=['GET', 'POST'])
def data():
    if request.method == 'POST':
        file = request.files['csvfile']
        if not os.path.isdir('static_file'):
            os.mkdir('static_file')
        filepath = os.path.join('static_file', file.filename)
        file.save(filepath)
        return 'the file name of the uploaded file is : {}'.format(file.filename)
    return render_template('upload.html')


@app.route('/dash', methods=['GET', 'POST'])
def dash():
    if request.method == 'POST':
        variable = request.form['variable']
        data = pd.read_csv('static_file/shampoo.csv')
        size = len(data[variable])
        pyplot.bar(range(size), data[variable])
        image_path = os.path.join('static_file', 'image'+'.png')
        pyplot.savefig(image_path)
        plt.show()
        return render_template('image.html', image=image_path)
    return render_template('dash.html')


if __name__ == '__main__':
    app.run(debug=True)
