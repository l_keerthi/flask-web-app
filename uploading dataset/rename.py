import os

os.chdir('E:\Desktop\semester 8\microsevices')

# Am I in the correct directory?
cwd = os.getcwd()
print("Current working directory:", cwd)

for f in os.listdir():
    f_name, f_ext = os.path.splitext(f)

    f_course, f_id, f_title = f_name.split('_')

    new_name = '{}-{}-{}{}'.format(f_title, f_course, f_id, f_ext)

    os.rename(f, new_name)


