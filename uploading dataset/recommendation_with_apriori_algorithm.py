#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pandas as pd
import numpy as np
from mlxtend.frequent_patterns import apriori, association_rules
import pickle

data = pd.read_excel("online_retail_II.xlsx")
data_cp = data.copy()
print(data.head())

print(data.info())

print(data.isna().sum())

data.dropna(inplace=True)

print(data.shape)

print(data.describe().T)

# Handling cancelled transactions
data_Invoice = pd.DataFrame({"Invoice":[row for row in data["Invoice"].values if "C"  not in str(row)]})
data_Invoice.head()
data_Invoice = data_Invoice.drop_duplicates("Invoice")

data = data.merge(data_Invoice, on = "Invoice")


def outlier_thresholds(dataframe, variable):
    quartile1 = dataframe[variable].quantile(0.01)
    quartile3 = dataframe[variable].quantile(0.99)
    interquantile_range = quartile3 - quartile1
    up_limit = quartile3 + 1.5 * interquantile_range
    low_limit = quartile1 - 1.5 * interquantile_range
    return low_limit, up_limit


# Replace outliers with thresholds
def replace_with_thresholds(dataframe, variable):
    low_limit, up_limit = outlier_thresholds(dataframe, variable)
    dataframe.loc[(dataframe[variable] < low_limit), variable] = low_limit
    dataframe.loc[(dataframe[variable] > up_limit), variable] = up_limit

data.dtypes

num_cols = [col for col in data.columns if data[col].dtypes in ["int64","float64"] and "ID" not in col]

print(num_cols)

for col in num_cols:
    replace_with_thresholds(data, col)

print(data.describe().T)

data = data[data["Quantity"] > 0]
data = data[data["Price"] > 0]

# Unique Number of Products (with Description)

data.Description.nunique()

# Unique Number of Products (with StockCode)

data.StockCode.nunique()

"""Duplication in the description for a stock code"""

data_product = data[["Description","StockCode"]].drop_duplicates()
data_product = data_product.groupby(["Description"]).agg({"StockCode":"count"}).reset_index()
data_product.sort_values("StockCode", ascending=False).head()

data_product.rename(columns={'StockCode':'StockCode_Count'},inplace=True)

data_product = data_product[data_product["StockCode_Count"]>1]

# Delete products with more than one stock code

data = data[~data["Description"].isin(data_product["Description"])]

print(data.StockCode.nunique())
print(data.Description.nunique())

data_product = data[["Description","StockCode"]].drop_duplicates()
data_product = data_product.groupby(["StockCode"]).agg({"Description":"count"}).reset_index()
data_product.rename(columns={'Description':'Description_Count'},inplace=True)

data_product = data_product.sort_values("Description_Count", ascending=False)
data_product.head()

data_product = data_product[data_product["Description_Count"] > 1] 

data_product.head()

# delete stock codes that represent multiple products
data = data[~data["StockCode"].isin(data_product["StockCode"])]

# Now each stock code represents a single product:

print(data.StockCode.nunique())
print(data.Description.nunique())

# The post statement in the stock code shows the postage cost, let's delete it as it is not a product

data = data[~data["StockCode"].str.contains("POST", na=False)]

# Creating germany data basket
data_germany = data[data["Country"] == "Germany"]
print(data_germany.shape)

"""# Preparing Invoice-Product Matrix fot ARL Data Structure

"""


def create_invoice_product_data(dataframe, id=False):
    if id:
        return dataframe.groupby(['Invoice', "StockCode"])['Quantity'].sum().unstack().fillna(0). \
            applymap(lambda x: 1 if x > 0 else 0)
    else:
        return dataframe.groupby(['Invoice', 'Description'])['Quantity'].sum().unstack().fillna(0). \
            applymap(lambda x: 1 if x > 0 else 0)


gr_inv_pro_data = create_invoice_product_data(data_germany, id=True)
gr_inv_pro_data.head()

# define a function to find the product name corresponding to the stock code:


def check_id(dataframe, stockcode):
    product_name = dataframe[dataframe["StockCode"] == stockcode]["Description"].unique()[0]
    return stockcode, product_name


check_id(data_germany, 10002)

check_id(data_germany, 15039)

"""# Determination of Association Rules

"""

frequent_itemsets = apriori(gr_inv_pro_data, min_support=0.05, use_colnames=True)
frequent_itemsets.head()

rules = association_rules(frequent_itemsets, metric="support", min_threshold=0.01)

rules.sort_values("support", ascending=False).head(5)

"""# Suggesting a Product to Users at the Basket Stage

"""

sorted_rules = rules.sort_values("lift", ascending=False)

"""If user buys a product whose id is 21731, which products do you recommend?"""

product_id = 21731

check_id(data, product_id)

# define a function for recommending a product:

product_id = 21731
recommendation_list = []


for idx, product in enumerate(sorted_rules["antecedents"]):
    for j in list(product):
        if j == product_id:
            recommendation_list.append(list(sorted_rules.iloc[idx]["consequents"])[0])
            recommendation_list = list( dict.fromkeys(recommendation_list) )

# Let's bring the top 3 most preferred products together with the product with id 21731.

list_top3 = recommendation_list[0:3]
list_top3

for elem in list_top3:
    print(check_id(data_germany,elem))

"""

---
"""

# Import & Filter Data:


def data_filter(dataframe, country=False, Country=""):
    if country:
        dataframe = dataframe[dataframe["Country"] == Country]
    return dataframe


def outlier_thresholds(dataframe, variable):
    quartile1 = dataframe[variable].quantile(0.01)
    quartile3 = dataframe[variable].quantile(0.99)
    interquantile_range = quartile3 - quartile1
    up_limit = quartile3 + 1.5 * interquantile_range
    low_limit = quartile1 - 1.5 * interquantile_range
    return low_limit, up_limit


def replace_with_thresholds(dataframe, variable):
    low_limit, up_limit = outlier_thresholds(dataframe, variable)
    dataframe.loc[(dataframe[variable] < low_limit), variable] = low_limit
    dataframe.loc[(dataframe[variable] > up_limit), variable] = up_limit
    
    
def data_prep(dataframe):
    
    # Data preprocessing:
    dataframe.dropna(inplace=True)
    
    # Delete if the product name contains "POST":
    dataframe = dataframe[~dataframe["StockCode"].str.contains("POST", na=False)]
    
    dataframe = dataframe[~dataframe["Invoice"].str.contains("C", na=False)]
    dataframe = dataframe[dataframe["Quantity"] > 0]
    dataframe = dataframe[dataframe["Price"] > 0]
    replace_with_thresholds(dataframe, "Quantity")
    replace_with_thresholds(dataframe, "Price")
    return dataframe

 
# Invoice Product Matrix:
def create_invoice_product_data(dataframe, id=False):
    if id:
        return dataframe.groupby(['Invoice', "StockCode"])['Quantity'].sum().unstack().fillna(0). \
            applymap(lambda x: 1 if x > 0 else 0)
    else:
        return dataframe.groupby(['Invoice', 'Description'])['Quantity'].sum().unstack().fillna(0). \
            applymap(lambda x: 1 if x > 0 else 0) 
    
    
# Find Product name with Stock Code:

def check_id(dataframe, stockcode):
    product_name = dataframe[dataframe["StockCode"] == stockcode]["Description"].unique()[0]
    return stockcode, product_name


# Apriori Algorithm & ARL Rules:

def apriori_alg(dataframe, support_val=0.01):
    inv_pro_data = create_invoice_product_data(dataframe, id=True)
    frequent_itemsets = apriori(inv_pro_data, min_support=support_val, use_colnames=True)
    rules = association_rules(frequent_itemsets, metric="support", min_threshold=support_val)
    sorted_rules =  rules.sort_values("support", ascending=False) 
    return sorted_rules
    

def recommend_product(dataframe, product_id, support_val= 0.01, num_of_products=5):
    sorted_rules = apriori_alg(dataframe, support_val)
    recommendation_list = []  
    for idx, product in enumerate(sorted_rules["antecedents"]):
        for j in list(product):
            if j == product_id:
                recommendation_list.append(list(sorted_rules.iloc[idx]["consequents"])[0])
                recommendation_list = list( dict.fromkeys(recommendation_list) )
    return(recommendation_list[0:num_of_products])


# Data Preparation: 
data = data_cp.copy()

data = data_prep(data)
data = data_filter(data,country=True,Country="Germany")
data.head()


def recommendation_system_func(dataframe,support_val=0.01, num_of_products= 5 ):
    product_id = input("Enter a product id:")
    
    if product_id in list(dataframe["StockCode"].astype("str").unique()):
        product_list = recommend_product(dataframe, int(product_id), support_val, num_of_products)
        if len(product_list) == 0:
            print("There is no product can be recommended!")
        else:
            print("Related products with product id -" , product_id , "can be seen below:")
        
            for i in range(0, len(product_list[0:num_of_products])):
                print(check_id(dataframe, product_list[i]))
            
    else:
        print("Invalid Product Id, try again!")

# Enter product id - 1 (22326)


recommendation_system_func(data)

# Enter product id - 2 (22725)

recommendation_system_func(data)

# Enter invalid product id 

recommendation_system_func(data)

# Enter invalid product id 

recommendation_system_func(data)