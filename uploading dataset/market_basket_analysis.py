#!/usr/bin/env python
# -*- coding: utf-8 -*-
import pandas as pd
import seaborn as sns
import matplotlib as mpl
import matplotlib.pyplot as plt
import self as self
from mlxtend.frequent_patterns import apriori
from mlxtend.frequent_patterns import association_rules
import pickle

df = pd.read_csv("OnlineRetail.csv", encoding='unicode_escape')

print(df.head())

df['Description'] = df['Description'].str.strip()
df.dropna(axis=0, subset=['InvoiceNo'], inplace=True)
df['InvoiceNo'] = df['InvoiceNo'].astype('str')
df = df[~df['InvoiceNo'].str.contains('C')]

basket = (df[df['Country'] == "France"]
          .groupby(['InvoiceNo', 'Description'])['Quantity']
          .sum().unstack().reset_index().fillna(0)
          .set_index('InvoiceNo'))

print(basket.head())


def encode_units(x):
    if x <= 0:
        return 0
    if x >= 1:
        return 1


basket_sets = basket.applymap(encode_units)
basket_sets.drop('POSTAGE', inplace=True, axis=1)

# Generate Frequent Itemsets


frequent_itemsets = apriori(basket_sets, min_support=0.07, use_colnames=True)

# Association Rules Using Frequent Itemset

rules = association_rules(frequent_itemsets, metric="lift", min_threshold=1)
print(rules.head())

sns.scatterplot(x="support", y="confidence", size="lift", data=rules)
print(plt.show())


rules[ (rules['lift'] >= 6) & (rules['confidence'] >= 0.8) ]

print("ALARM CLOCK BAKELIKE GREEN : ", basket['ALARM CLOCK BAKELIKE GREEN'].sum())

print("ALARM CLOCK BAKELIKE RED : ", basket['ALARM CLOCK BAKELIKE RED'].sum())

"""For instance, we can see that we sell 340 Green Alarm clocks but only 316 Red Alarm Clocks

`So maybe we can drive more Red Alarm Clock sales through recommendations`
"""

basket2 = (df[df['Country'] == "Germany"]
          .groupby(['InvoiceNo', 'Description'])['Quantity']
          .sum().unstack().reset_index().fillna(0)
          .set_index('InvoiceNo'))


print(basket2.head())

basket_sets2 = basket2.applymap(encode_units)
basket_sets2.drop('POSTAGE', inplace=True, axis=1)
frequent_itemsets2 = apriori(basket_sets2, min_support=0.05, use_colnames=True)
rules2 = association_rules(frequent_itemsets2, metric="lift", min_threshold=1)

sns.scatterplot(x="support", y="confidence",  size="lift", data=rules2)
print(plt.show())

rules2[ (rules2['lift'] >= 4) & (rules2['confidence'] >= 0.5)]

# Transform the DataFrame of rules into a matrix using the lift metric

pivot = rules2.pivot(index='consequents', columns='antecedents', values='lift')

# Generate a heatmap with annotations on and the colorbar off

sns.heatmap(pivot, annot = True, cbar=True)
plt.yticks(rotation=0)
plt.xticks(rotation=90)
print(plt.show())

# Transform the DataFrame of rules into a matrix using the confidence metric

pivot = rules2.pivot(index='consequents', columns='antecedents', values='confidence')

# Generate a heatmap 
sns.heatmap(pivot, annot=True, cbar=True)
plt.yticks(rotation=0)
plt.xticks(rotation=90)
print(plt.show())

pickle.dump(rules2.pivot, open('MBAmodel.pkl', 'wb'))

# Reloading the model object
model = pickle.load(open('MBAmodel.pkl', 'rb'))
print(model.pivot([[536370, 0.763158]]))

