#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Commented out IPython magic to ensure Python compatibility.
from math import ceil

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn import metrics
import time, warnings
import datetime as dt
from matplotlib import pylab
from matplotlib import pyplot
from sklearn.model_selection import train_test_split
import statsmodels.api as sm
from statsmodels.tsa.arima.model import ARIMA
from statsmodels.tsa.stattools import adfuller
from statsmodels.graphics.tsaplots import plot_pacf, plot_acf
from statsmodels.tsa.arima_model import ARMA
import plotly
plotly.offline.init_notebook_mode(connected=True)
from ipywidgets import interact, interactive, fixed, interact_manual,VBox,HBox,Layout
import ipywidgets as widgets

retail = pd.read_csv("OnlineRetail.csv", encoding = 'unicode_escape')
print(retail.head())

print(retail.info())
retail.shape

retail.isnull().sum().sort_values(ascending = False)

print(retail.describe())

retail.dtypes

print(retail['Country'].value_counts().head(20))

# Getting some more general information about the data
print("Number of transactions: ", retail['InvoiceNo'].nunique())
print("Number of products bought: ",retail['StockCode'].nunique())
print("Number of customers:", retail['CustomerID'].nunique() )
print("Percentage of customers NA: ", round(retail['CustomerID'].
isnull().sum() * 100 / len(retail),2),"%" )
print('Number of countries: ',retail['Country'].nunique())

# Checking for Missing Values
print(retail.isnull().sum())

df = retail.drop(columns=['CustomerID'])

# Checking to see if NaN values were filtered out
df.isnull().sum()

df['Description'] = df['Description'].fillna('UNKNOWN ITEM')

# Checking to see if NaN values were filtered out
df.isnull().sum()

df['Description'] = df['Description'].str.upper()
df['Description'].value_counts().head()

print(df.describe())

# Exploring the negative Quantity and UnitePrice
df[df['Quantity'] < 0].head(10)

df = df[df['Quantity'] > 0]

df.describe()

# Negative UnitPrice so let's filter out them
df = df[df['UnitPrice'] > 0]
df.describe()

# Checking for duplicates

print("Number of duplicated transactions:", len(df[df.duplicated()]))

# Lets remove the duplicates
df.drop_duplicates(inplace = True)

# Checking for duplicates

print("Number of duplicated transactions:", len(df[df.duplicated()]))

print(df.shape)

"""Creating Features"""

# Creating a another new column with the total value of each order
df['Total_Sales_Amount'] = df['Quantity'] * df['UnitPrice']

# sorting the dataset by sales amount
df.sort_values(by = 'Total_Sales_Amount')

df['InvoiceDate']=pd.to_datetime(df['InvoiceDate'])
df['Year']=df.InvoiceDate.dt.year
df['Month']=df.InvoiceDate.dt.month
df['Week']=df['InvoiceDate'].dt.week
df['Year_Month']=df.InvoiceDate.dt.to_period('M')
df['Hour']=df.InvoiceDate.dt.hour
df['Day']=df.InvoiceDate.dt.day
df['WeekDay'] = df.InvoiceDate.dt.day_name()
df['Quarter'] = df.Month.apply(lambda m:'Q'+str(ceil(m/4)))
df['Date']=pd.to_datetime(df[['Year','Month','Day']])

df.head(10)

print(df.dtypes)

top_products = df['Description'].value_counts()[:15]
print(top_products)

dfp1 = df[df['Description'] == 'WHITE HANGING HEART T-LIGHT HOLDER']
dfp1.head()

# Country ordered the most
df['Country'].value_counts().head(12).plot.bar(figsize = (15, 7))
plt.title('Top Online Retail Market', fontsize = 14)
plt.xlabel('Names of Countries')
plt.ylabel('Count')
plt.xticks(fontsize = 10)
plt.yticks(fontsize = 10)
print(plt.show())

# Top stockcodes on the dataset
color = plt.cm.copper(np.linspace(0, 1, 20))
df['StockCode'].value_counts().head(20).plot.bar(color = color, figsize = (18, 8))
plt.title('Most Popular Stock codes', fontsize = 16)
plt.xticks(fontsize = 12)
plt.yticks(fontsize = 12)
plt.xlabel('Stock Codes', fontsize=10)
plt.ylabel('Quantity Sold', fontsize=10)
print(plt.show())


# Top selling products
top_products = df['Description'].value_counts()[:15]
plt.figure(figsize=(10,6))
sns.set_context("paper", font_scale=1.5)
sns.barplot(y = top_products.index, x = top_products.values)
plt.title("Top Selling Products")
plt.xticks(fontsize = 15)
print(plt.show())

sales_by_weekday = df.groupby(by='WeekDay')['Total_Sales_Amount'].sum().reset_index()

sales_by_weekday.plot.bar(figsize = (12, 6))
plt.title('Weekly Sales', fontsize = 16)
print(plt.show())

# Orders on weekdays
Dy = pd.DataFrame(df.groupby(['WeekDay'])['InvoiceNo'].count())
Dy = Dy.reindex(['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday','Saturday','Sunday']).reset_index
Dy

P1 = pd.DataFrame(df.groupby(['WeekDay'])['InvoiceNo'].count()).reset_index()
ax = sns.barplot(x="WeekDay", y="InvoiceNo", data = P1)
ax.set_title('Top Orders During the Weekdays')
print(plt.show())

P2 = pd.DataFrame(df.groupby(['Hour'])['InvoiceNo'].count()).reset_index()
ax = sns.barplot(x= 'Hour', y= 'InvoiceNo', data = P2)
ax.set_title('Transactions Per Hour')
print(plt.show())

P3 = pd.DataFrame(df.groupby(['Date'])['InvoiceNo'].count()).reset_index()
plt.figure(figsize=(15, 6))
ax = sns.lineplot(x="Date", y="InvoiceNo", data = P3)

"""*   More purchasing made at the end of the year  
*   Upward trend during the year especially close to End of the year(Holiday season)

**We can try to Forecast the trend so it can prepare the company for Inventory management and sales.**

--- 
---
"""

# Weekly Sales

ds_weekly = df.groupby(by=['Year', 'Week'])['Total_Sales_Amount'].sum().reset_index()
print(ds_weekly.head())

roll_mean = ds_weekly.rolling(window=3, center=False).mean()
roll_std = ds_weekly.rolling(window=3, center=False).std()

fig = plt.figure(figsize=(12, 7))
plt.plot(ds_weekly, color='blue', label='Original')
plt.plot(roll_mean, color='red', label='Rolling Mean')
plt.plot(roll_std, color='black', label = 'Rolling Std')
plt.legend(loc='best')
plt.title('Rolling Mean & Standard Deviation')
print(plt.show(block=False))

fig = pd.DataFrame(ds_weekly.groupby(['Week'])['Total_Sales_Amount'].count()).reset_index()
print(plt.figure(figsize=(18, 6)))
ax = sns.lineplot(x="Week", y="Total_Sales_Amount", data = ds_weekly)

# Weekly Sales: Test of Stationarity of Actual Series

output = adfuller(ds_weekly.Total_Sales_Amount)
print('***************************Week*********************************')
print('ADF Statistic: {0:.2f} and P value:{1:.5f}'.format(*output))
print("As we can see the p value is extreemly high which indicates that we are fail to reject null hypothesis " \
"and can conclude that series is not stationary ")

# Test of Stationarity with 1 differencing of series.
d=1
print('***************************Week*********************************')
series = ds_weekly.Total_Sales_Amount.diff(d)
series = series.dropna()
output = adfuller(series)
print('ADF Statistic: {0:.2f} and P value:{1:.5f}'.format(*output))

fig, ax = plt.subplots(1, 2, figsize=(15, 5))
plot_acf(series, ax=ax[0])
plot_pacf(series, ax=ax[1])
print(plt.show())

# Weekly Sales: Train & Test Split

series=ds_weekly.Total_Sales_Amount
split_time = 45
time=np.arange(len(ds_weekly))
xtrain=series[:split_time]
xtest=series[split_time:]
timeTrain = time[:split_time]
timeTest = time[split_time:]

# Fitting the ARIMA model

model = ARIMA(xtrain, order=(1, 1, 0))
model_fit = model.fit()
print(model_fit.summary())

# Weekly Sales Trend and Forecast

ytrain_pred = model_fit.predict()
ytest_pred = model_fit.predict(start=min(timeTest),end=max(timeTest),dynamic=True)
print('RMSE Train :', np.sqrt(np.mean((ytrain_pred - xtrain)**2)))
print('RMSE Test :', np.sqrt(np.mean((ytest_pred - xtest)**2)))
forecast = model_fit.forecast(20, alpha=0.05)
print('Weekly Sales Trend and Forecast')
model_fit.plot_predict(1, 47)

model_fit.forecast(30)[0]

"""**Product #1: 'White Hanging Heart T-Light Holder**"""

ds_weekly_P1 = dfp1.groupby(by=['Year','Week'])['Total_Sales_Amount'].sum().reset_index()

print(ds_weekly_P1.head())

# Product #1: Total Sales Weekly Trend
plt.figure(figsize=(18,6))
plt.title('Product #1: Total Sales Weekly Trend', fontsize = 16)
ax = sns.lineplot(x="Week", y="Total_Sales_Amount", data = ds_weekly_P1)

# Product #1: Test of Stationarity with 1 differencing of series

d=1
print('***************************Week*********************************')
series_1 = ds_weekly_P1.Total_Sales_Amount.diff(d)
series_1 = series_1.dropna()
output = adfuller(series_1)
print('ADF Statistic: {0:.2f} and P value:{1:.5f}'.format(*output))

# Product #1: PACF & ACF
fig, ax = plt.subplots(1, 2, figsize=(15, 5))
plot_acf(series_1, ax=ax[0])
plot_pacf(series_1, ax=ax[1])
print(plt.show())

# Product #1: Train & Test Split
series_1=ds_weekly_P1.Total_Sales_Amount
split_time = 42
time=np.arange(len(ds_weekly_P1))
xtrain=series_1[:split_time]
xtest=series_1[split_time:]
timeTrain = time[:split_time]
timeTest = time[split_time:]

model = ARIMA(xtrain, order=(1, 1, 0))
model_fit = model.fit()
print(model_fit.summary())

# Product #1: Weekly Sales Trend and Forecast

ytrain_pred = model_fit.predict()
ytest_pred = model_fit.predict(start=min(timeTest), end=max(timeTest), dynamic=True)
print('RMSE Train :', np.sqrt(np.mean((ytrain_pred - xtrain)**2)))
print('RMSE Test :', np.sqrt(np.mean((ytest_pred - xtest)**2)))
forecast = model_fit.forecast(20, alpha=0.05)
model_fit.plot_predict(1, 44)

# Save the model object
import pickle
pickle.dump(model, open('arimamodel.pkl', 'wb'))

# Reloading the model object
model = pickle.load(open('arimamodel.pkl', 'rb'))
print(model.predict([[1, 44]]))

